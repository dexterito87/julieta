from django import forms
from django.forms import ModelForm
from .models import Product, Todo


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'


class TodoForm(ModelForm):
    class Meta:
        model = Todo
        fields = '__all__'





