from django.contrib import admin

# Register your models here.


from .models import Product, Todo, Photo, Category

admin.site.register(Product)
admin.site.register(Todo)
admin.site.register(Category)
admin.site.register(Photo)



