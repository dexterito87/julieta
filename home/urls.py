from django.urls import path
from . import views

urlpatterns = [
    path('', views.home),
    path('storage/', views.storage, name='storage'),
    path('todo/', views.todo, name='todo'),
    path('storage/edit/<str:pk>/', views.edit_storage, name='edit'),
    path('storage/delete/<str:pk>/', views.delete_storage, name='delete'),
    path('todo/complete/<str:pk>/', views.complete_todo, name='complete'),
    path('gallery/', views.gallery, name='gallery'),
    path('photo/<str:pk>/', views.viewPhoto, name='photo'),
    path('add/', views.addPhoto, name='add'),
    path('photo/delete/<str:pk>/', views.delete_photo, name='delete'),
    path('todo/edit/<str:pk>/', views.edit_todo, name='edit_todo'),
    path('todo/delete/<str:pk>/', views.delete_todo, name='delete')
]
