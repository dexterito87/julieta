from django.shortcuts import render, redirect
from django.contrib import messages

# Create your views here.
from home.models import Product, Todo, Photo, Category
from .forms import ProductForm, TodoForm


def home(request):
    return render(request, 'accounts/main.html')


def storage(request):
    form = ProductForm()
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            product = form.save(commit=False)
            if product.square_meters is None and product.width is not None and product.length is not None:
                product.square_meters = product.width * product.length
            product.save()

            messages.success(request, 'Продукта е добавен')
        else:
            messages.success(request, 'Продукта не е добавен')
    products = Product.objects.all()
    return render(request, 'accounts/storage.html', {'products': products, 'form': form})


def todo(request):
    form = TodoForm()
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            todo = form.save(commit=False)
            todo.save()
    todos = Todo.objects.all()
    context = {'todos': todos, 'form': form}
    return render(request, 'accounts/todo.html', context)


def edit_storage(request, pk):
    product = Product.objects.get(pk=pk)
    form = ProductForm()
    if request.method == 'POST':
        form = ProductForm(request.POST, instance=product)
        if form.is_valid():
            product = form.save(commit=False)
            product.save()
            messages.success(request, 'Продукта е редактиран')
            return redirect('/storage')
        else:
            messages.success(request, 'Продукта не е редактиран')
    return render(request, 'accounts/storage_edit.html', {'product': product, 'form': form})


def delete_storage(request, pk):
    product = Product.objects.get(pk=pk)
    product.delete()
    messages.success(request, 'Продукта е Изтрит')
    return redirect('/storage')


def complete_todo(request, pk):
    todo = Todo.objects.get(pk=pk)
    todo.complete = True
    todo.save()
    return redirect('/todo')


def gallery(request):
    category = request.GET.get('category')
    if category == None:
        photos = Photo.objects.all()
    else:
        photos = Photo.objects.filter(category__name=category)

    categories = Category.objects.all()
    context = {'categories': categories, 'photos': photos}
    return render(request, 'gallery/gallery.html', context)


def viewPhoto(request, pk):
    photo = Photo.objects.get(id=pk)
    return render(request, 'gallery/photo.html', {'photo': photo})


def addPhoto(request):
    categories = Category.objects.all()

    if request.method == 'POST':
        data = request.POST
        images = request.FILES.getlist('images')

        if data['category'] != 'none':
            category = Category.objects.get(id=data['category'])
        elif data['category_new'] != '':
            category, created = Category.objects.get_or_create(
                name=data['category_new'])
        else:
            category = None

        for image in images:
            photo = Photo.objects.create(
                category=category,
                description=data['description'],
                image=image,
            )

        return redirect('gallery')

    context = {'categories': categories}
    return render(request, 'gallery/add.html', context)


def delete_photo(request, pk):
    photo = Photo.objects.get(pk=pk)
    photo.delete()
    return redirect('/gallery')


def edit_todo(request, pk):
    todo = Todo.objects.get(pk=pk)
    form = TodoForm()
    if request.method == 'POST':
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save(commit=False)
            todo.save()
            return redirect('/todo')
    return render(request, 'accounts/todo_edit.html', {'todo': todo, 'form': form})


def delete_todo(request, pk):
    todo = Todo.objects.get(pk=pk)
    todo.delete()
    return redirect('/todo')
